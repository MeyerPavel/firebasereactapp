import React, { Component } from 'react';
import fire from './config/Fire';
import CoinTable from './components/coin-table'
const database = fire.database();
var ref = database.ref('users');
ref.on('value', gotData,errData);
var dataJSON = [];

function gotData(dataDB){
    var users = dataDB.val();
    if(users != null){
        dataJSON = [];
        var keys = Object.keys(users);
        for (var i = 0; i < keys.length; i ++){
            var k = keys[i];  
            dataJSON[i] = {
                "email" : users[k].email,
                "lastLoginTime" : users[k].lastLoginTime,
                "registrationDate" : users[k].registrationDate,
                "userState": users[k].userState,
                "key" : k
            }
        }
    }
}

function errData(err){
    console.log("Error!");
    console.log(err);

}

class Home extends Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.state = {
            data: dataJSON,
            }
    }

    handleChange(e) {
        this.setState({ data: dataJSON });
    }

    render() {
        var Data = {
            dataDB : this.state.data,
            handler : this.handleChange
        }
        return (
            <div>
                <h1>Welcome to Home</h1>
                <button onClick={this.props.updateState} className="btn btn-primary" style={{marginBottom: '25px'}} type="submit">Logout</button>
                <CoinTable data={Data}/> 
            </div>
        );
    }
}

export default Home;