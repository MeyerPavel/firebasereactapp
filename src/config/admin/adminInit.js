import admin from "firebase-admin";

var serviceAccount = require("./ServiceAccountKey.json");

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://meyertask2.firebaseio.com"
});

