import React, { Component } from 'react'
import ReactDOM from 'react-dom';
import fire from 'C:/Users/ASUS/Desktop/JavaScript/Lessons/task-2/src/config/Fire';
const database = fire.database();
var ref = database.ref('users');

class CoinTable extends Component {
    constructor(props) {
        super(props);
        this.blockUser = this.blockUser.bind(this);
        this.deleteUser = this.deleteUser.bind(this);
        this.selectAllUsers = this.selectAllUsers.bind(this);
    }


    blockUser(e){
        var users = [];
        for(var i = 0;i < this.props.data.dataDB.length;i++){
            var checkBox = document.getElementById(this.props.data.dataDB[i].key);
            if (checkBox.checked){
                users.push(this.props.data.dataDB[i]);
            }
        }
            for(var i = 0; i < users.length; i++){
                var status;
                if((users[i].userState === "active"))
                    status = "ban";
                else{
                    status = "active";
                }
            var Data = {
                userState: status
            }
            ref.child(users[i].key).update(Data);
        }
        this.props.data.handler(e);
    }

    deleteUser(e) {
        var users = [];
        for(var i = 0;i < this.props.data.dataDB.length;i++){
            var checkBox = document.getElementById(this.props.data.dataDB[i].key);
            if (checkBox.checked){
                users.push(this.props.data.dataDB[i]);
            }
        }
        for(var i = 0; i < users.length; i++){
                document.getElementById(this.props.data.dataDB[i].key).checked = false;
                ref.child(users[i].key).remove();
            }
        this.props.data.handler(e);
    }
    
    selectAllUsers() {
            for(var i = 0;i < this.props.data.dataDB.length;i++){
                document.getElementById(this.props.data.dataDB[i].key).checked = true;
            }
    }

    render() {
        return (
            <div>
                <div className="btn-group" role="group" aria-label="Basic example">
                    <button type="button" className="btn btn-secondary" onClick={this.blockUser}>Block</button>
                    <button type="button" className="btn btn-secondary" onClick={this.deleteUser}>Delete</button>
                    <button type="button" className="btn btn-secondary"onClick={this.selectAllUsers} >Select All</button>
                </div>
            <table className="table table-striped table-bordered table-sm">
                <thead>
                    <tr>
                        <th>Choose</th>
                        <th>email</th>
                        <th>Last Login</th>
                        <th>Registration Date</th>
                        <th>Status</th>
                        
                    </tr>
                </thead>
                <tbody>
                    {
                        this.props.data.dataDB.map(row => (
                            <tr>
                                <td>
                                    <input id={row.key} type="checkbox" ></input>
                                </td>
                                <td>{row.email}</td>
                                <td>{row.lastLoginTime}</td>
                                <td>{row.registrationDate}</td>
                                <td>{row.userState}</td>
                            </tr>
                        ))
                    }
                </tbody>
            </table>
            </div>
        ) 
    }
}

export default CoinTable;