import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import fire from './config/Fire';
import Home from './Home';

const database = fire.database();
const active = "active";
var ref = database.ref('users');
ref.on('value', gotData,errData);
var bannedUsers= [];
var registeredUsers = new Map();
var mapKeys = new Map();

function gotData(data){
    bannedUsers = [];
    var users = data.val();
    if(users != null){
        var keys = Object.keys(users);
        for (var i = 0; i < keys.length; i ++){
            var k = keys[i];  
            var email = users[k].email;
            var password = users[k].password;
            if(!searchRegisteredUser(email,registeredUsers)){
                registeredUsers.set(email,password);
            }
            mapKeys.set(email,k);
            if(users[k].userState !== "active") bannedUsers.push(email);
        }
    }
}

function errData(err){
    console.log("Error!");
    console.log(err);

}

function searchRegisteredUser(userEmail, registeredUsers){
    for (var i=0; i < registeredUsers.length; i++) {
        if (registeredUsers[i].login === userEmail) {
            return true;
        }
    }
    return false;
}

var today = new Date(),
currentDate = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate()+ ' '+ today.getHours()+':'+ today.getMinutes()+':'+ today.getSeconds();


class Login extends Component {
    constructor(props) {
        super(props);
        this.login = this.login.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.signup = this.signup.bind(this);
        this.state = {
        email: '',
        password: '',
        isSignUp: false
        };
    }

    handleChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }

    login(e) {
        if(!bannedUsers.includes(this.state.email) && (registeredUsers.get(this.state.email) === this.state.password)){
            bannedUsers = [];
            registeredUsers = new Map();
            e.preventDefault();
            currentDate = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate()+ ' '+ today.getHours()+':'+ today.getMinutes()+':'+ today.getSeconds();
            var Data = {
                lastLoginTime: currentDate,
            }
            ref.child(mapKeys.get(this.state.email)).update(Data);
            this.props.updateState(e);
        }else{
            e.preventDefault();
            if(bannedUsers.includes(this.state.email))
                alert("BANNED");
            else
                alert("This login is not registered");
        }
    }

    signup(e){
        if(!registeredUsers.has(this.state.email)){
        e.preventDefault();
            var Data = {
                email: this.state.email,
                password: this.state.password,
                registrationDate: currentDate,
                lastLoginTime: currentDate,
                userState: active
            }
            ref.push(Data);
        }else{
            e.preventDefault();
            alert("This email is registered")
        }
    }
    
    render() {
        return (
            <div className="col-md-6">
                <form>
            <div className="form-group">
                <label htmlFor="exampleInputEmail1">Email address</label>
                <input value={this.state.email} onChange={this.handleChange} type="email" name="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" />
                <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
            </div>
            <div className="form-group">
                <label htmlFor="exampleInputPassword1">Password</label>
                <input value={this.state.password} onChange={this.handleChange} type="password" name="password" className="form-control" id="exampleInputPassword1" placeholder="Password" />
            </div>
                <button type="submit" onClick={this.login} className="btn btn-primary">Login</button>
                <button type="submit" onClick={this.signup} style={{marginLeft: '25px'}} className="btn btn-success">Signup</button>
                </form>
            </div>
        );
    }
}
export default Login;