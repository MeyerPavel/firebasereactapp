import React, { Component } from 'react';
import fire from './config/Fire';
import Home from './Home';
import Login from './Login';

class App extends Component {
  constructor() {
    super();
    this.state = ({
      isSignUp: false,
    });
    this.updateState = this.updateState.bind(this);
  }

  updateState(){
    this.setState({isSignUp : !this.state.isSignUp});
  }

  render() {
    return (
      <div>{this.state.isSignUp ? ( <Home updateState ={this.updateState}/>) : (<Login updateState ={this.updateState}/>)}</div>
    );
  }

}

export default App;